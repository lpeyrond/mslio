#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <sys/stat.h>
#include <math.h>
#include <cstdlib>
#include <ctime>
#include <fstream>  
#include <sys/time.h> 
#include <string.h>
#include <mpi.h>
#include <omp.h>

//Kernel options
#define USE_BUFFERING           true
#define BUFFER_SIZE             20000        
#define USE_MPI_IO              true            
#define SET_MPI_IO_COLLECTIVE   false           
#define USE_BINARY              true
#define LOG_TIMES               true            
#define LOG_PRECISE_WRITE       true            
#define LOGS_NAME               "kernelLogs"
#define LOW_RAM                 false            
#define SOLUTION_NAME           "solution"
#define STRIPE_PADDING          true
#define STRIPE_SIZE             512*1024        


#define FORMATED_DOUBLE_LENGHT 13

#define __S_IREAD 0400
#define __S_IWRITE 0200
#define __S_IEXEC 0100
#define S_IRWXU (__S_IREAD|__S_IWRITE|__S_IEXEC)


typedef struct parameters {
    int ccross;                     //
    int subelem;                    //
    unsigned long nbSolutionDouble; //Total number of double in each local problem file
    unsigned long nbLocalProblems;  //Total number of local problems
    char *mainPath;                 //Path to create the solutions
    char *logPath;                  //Path to store the log file
    int rank;                       //Rank of the mpi process
    int totalRank;                  //Total number of mpi ranks
    int logsSize;                   //Length of the logs array
    int stripeCountPerProblem;      //how many stripes are needed for the solution of 1 local problem
} * parameters_t ;

typedef struct buffer {
    char *data;
    uint size;                      //Current size of the buffer
    uint previous;                  //When emptying the buffer we add the current size to this field. It's used to know the writing offset inside a local problem.  
    uint max;                       //Maximum size of the buffer
} buffer ;


enum typeIO {
    OPEN,
    CLOSE,
    WRITE
};

//define an IO operation with the start time, end time, elapsed time, process rank that made it and its type (close, open, write)
//All times are in seconds
typedef struct operationIO {
    double startTime; 
    double endTime; 
    double elapsedTime;
    double writeTime;       //actual write time without array access, buffers memcpy ect ... 
    int localProblemId;     
    uint write_size;        //total size in bytes 
    uint repetition;        //number of time the operation was repeated (useful to know how many writes were done to write a single file)
    int threadId;           //thread that made the operation
    int processRank;        //process that made the operation
    typeIO type;            //type of io operation
} opIO;

void errorExit(int line, const char * fileName, const char * errorMessage){
    fprintf(stderr, "[ %s %d ] Error : %s \n", fileName, line, errorMessage);
    exit(EXIT_FAILURE);
}

double randomDouble(double upper){
    return (static_cast <double> (rand()) / (static_cast <double> (RAND_MAX/upper)));
}

void addLog(opIO *logs, int processProblemIndex, double timeStart, double timeEnd, parameters_t param, int threadId, int localProblemId, uint write_size, uint repetition, typeIO type, double writeTime){
    int logIndex = processProblemIndex;
    if (USE_MPI_IO){
        if (type == WRITE) logIndex += 1; //offset because the first element is the file open
        else if (type == CLOSE) logIndex = param->logsSize - 1;
        else if (type == OPEN) logIndex = 0;
    }else{
        logIndex = logIndex*3;
        if (type==WRITE) logIndex +=1;
        else if (type==CLOSE) logIndex +=2;
    } 

    if (type==WRITE){
        if (USE_BINARY) write_size *= sizeof(double);
        else write_size *= FORMATED_DOUBLE_LENGHT;
    }

    opIO *log = logs + logIndex;
    log->elapsedTime = timeEnd - timeStart;
    log->endTime = timeEnd;
    log->startTime = timeStart;
    log->localProblemId = localProblemId;
    log->repetition = repetition;
    log->threadId = threadId;
    log->type = type;
    log->write_size = write_size;
    log->processRank = param->rank;
    log->writeTime = writeTime;
}

double **createRandomArray(parameters_t param){
    int arraySize; 
    if (LOW_RAM){
        arraySize = 1;
    }else{
        arraySize = param->nbLocalProblems/param->totalRank;
    }

    double **arrayNumber = (double **) calloc(sizeof(double*), arraySize);
    if (!arrayNumber) errorExit(__LINE__, __FILE__, "Malloc Failed for the double array alloction");

    for(int i = 0; i< arraySize; i++){
        arrayNumber[i] = (double *) malloc(sizeof(double) * param->nbSolutionDouble);
        if (!arrayNumber[i]) errorExit(__LINE__, __FILE__, "Malloc Failed");

        for(int j =0; j< param->nbSolutionDouble; j++){
            arrayNumber[i][j] = randomDouble(9.0);
        }
    }
    return arrayNumber;
}

void destroyArray(double **arr, parameters_t param){
    int arraySize = param->nbLocalProblems/param->totalRank;
    if (LOW_RAM) arraySize = 1;
    for(int i = 0; i< arraySize; i++) 
        free(arr[i]);
    free(arr);
}

MPI_Offset getOffset(parameters_t param, int localProblemId, buffer *buff){
    MPI_Offset localProblemStart;
    
    if (STRIPE_PADDING) localProblemStart = param->stripeCountPerProblem * STRIPE_SIZE * localProblemId;
    else if (USE_BINARY) localProblemStart = sizeof(double) * param->nbSolutionDouble * localProblemId; 
    else localProblemStart = FORMATED_DOUBLE_LENGHT * param->nbSolutionDouble * localProblemId; 

    return localProblemStart + buff->previous; 
}

void writeSolution(FILE *fnsca, double d, double *writeTime, char *numberString, buffer *buff, int line, parameters_t param, int *writeCount, MPI_File fh, int localProblemId, int threadId){
    double timeStart, timeEnd;
    MPI_Offset offset;


    //All the cases using a buffer
    if (USE_BUFFERING){
        if (USE_BINARY){
            memcpy(buff->data + buff->size, &d, sizeof(double));
            buff->size += sizeof(double);
        }else{
            if (d<0) sprintf(numberString, "\n%2.5e",d);
            else sprintf(numberString, "\n %2.5e",d);
            memcpy(buff->data + buff->size, numberString, FORMATED_DOUBLE_LENGHT);
            buff->size += FORMATED_DOUBLE_LENGHT;
        }


        //empty the buffer to the file if the buffer is full or it's the end of the local problem solution
        if ((buff->size >= buff->max) || ((line+1) == param->nbSolutionDouble)){
            if(!USE_MPI_IO){
                if (LOG_PRECISE_WRITE) timeStart = omp_get_wtime();
                fwrite(buff->data, buff->size, 1, fnsca);
                if (LOG_PRECISE_WRITE) timeEnd = omp_get_wtime();
            }

            if (USE_MPI_IO){
                int error;
                offset = getOffset(param, localProblemId, buff);
                if (LOG_PRECISE_WRITE) timeStart = omp_get_wtime();

                if (SET_MPI_IO_COLLECTIVE){
                    if (USE_BINARY) error = MPI_File_write_at_all(fh, offset, buff->data, buff->size, MPI_BYTE, MPI_STATUS_IGNORE);
                    else error = MPI_File_write_at_all(fh, offset, buff->data, buff->size, MPI_CHAR, MPI_STATUS_IGNORE);
                }else{
                    if (USE_BINARY) error = MPI_File_write_at(fh, offset, buff->data, buff->size, MPI_BYTE, MPI_STATUS_IGNORE);
                    else error = MPI_File_write_at(fh, offset, buff->data, buff->size,  MPI_CHAR, MPI_STATUS_IGNORE);
                }

                if (LOG_PRECISE_WRITE) timeEnd = omp_get_wtime();
                if (error != MPI_SUCCESS) errorExit(__LINE__, __FILE__, "MPI write failed");
            }

            if (LOG_PRECISE_WRITE) *writeTime += timeEnd - timeStart;
            *writeCount += 1;
            buff->data[0] = '\0';
            buff->previous += buff->size;
            buff->size=0;
        }
    }


    if(!USE_MPI_IO){
        if (LOG_PRECISE_WRITE) timeStart = omp_get_wtime();

        if (USE_BINARY) fwrite(&d, sizeof(d), 1, fnsca);
        else fprintf(fnsca,"\n %2.5e", d); 
        
        if (LOG_PRECISE_WRITE) timeEnd = omp_get_wtime();
    }



    if(USE_MPI_IO && USE_BINARY){
        int error;
        offset = sizeof(double) * param->nbSolutionDouble * localProblemId + line * sizeof(d);

        if (LOG_PRECISE_WRITE) timeStart = omp_get_wtime();

        if(SET_MPI_IO_COLLECTIVE) error = MPI_File_write_at_all(fh, offset, &d, 1, MPI_DOUBLE, MPI_STATUS_IGNORE);
        else error = MPI_File_write_at(fh, offset, &d, 1, MPI_DOUBLE, MPI_STATUS_IGNORE);
        
        if (LOG_PRECISE_WRITE) timeEnd = omp_get_wtime();
        if (error != MPI_SUCCESS) errorExit(__LINE__, __FILE__, "MPI write failed");
    }


    if(USE_MPI_IO && !USE_BINARY){
        int error;
        offset = FORMATED_DOUBLE_LENGHT * param->nbSolutionDouble * localProblemId + line * FORMATED_DOUBLE_LENGHT;

        if(d<0) sprintf(numberString, "\n%2.5e",d);
        else sprintf(numberString, "\n% 2.5e",d);

        if (LOG_PRECISE_WRITE) timeStart = omp_get_wtime();

        if(SET_MPI_IO_COLLECTIVE) error = MPI_File_write_at_all(fh, offset, numberString, FORMATED_DOUBLE_LENGHT, MPI_CHAR, MPI_STATUS_IGNORE);
        else error = MPI_File_write_at(fh, offset, numberString, FORMATED_DOUBLE_LENGHT, MPI_CHAR, MPI_STATUS_IGNORE);

        if (LOG_PRECISE_WRITE) timeEnd = omp_get_wtime();
    }


    if (!USE_BUFFERING){
        if (LOG_PRECISE_WRITE) *writeTime += timeEnd - timeStart;
        *writeCount += 1;
    }
}

void solutionLocalProblem(MPI_File fh, opIO *logs, parameters_t param, double *arrayNumber, int localProblemId, int processProblemIndex, buffer *buff, int threadId){
    double timeStart, timeEnd;
    double writeTime= 0.0, openTime= 0.0, closeTime= 0.0 ;
    int writeCount=0, openCount=0, closeCount=0;
    FILE* fnsca;
    std::string localPath;

    //opens the local problem solution file because without MPI IO each local problems has its own solution file
    if (!USE_MPI_IO){
        localPath = param->mainPath + std::to_string(localProblemId);
        localPath += '/';
        localPath += SOLUTION_NAME;
        localPath += ".nsca";
        timeStart = omp_get_wtime();
        fnsca = fopen(localPath.c_str(),"w");
        timeEnd = omp_get_wtime();
        if(!fnsca) errorExit(__LINE__, __FILE__, "Single solution file failed opening");
        openCount++;
        if (LOG_TIMES) addLog(logs, processProblemIndex, timeStart, timeEnd, param, threadId, localProblemId, 0, 1, OPEN, 0);
    }

    int nValues = 1, dim=0;
    MPI_Offset offset;
    char stringNumber[15];
    double d;
    timeStart = omp_get_wtime();
    //take a double from the array and write it to the solution file
    for (;dim<nValues;dim++)
    for (int i=0;i<param->nbSolutionDouble;i++){
        d = arrayNumber[nValues*i+dim];
        writeSolution(fnsca, d, &writeTime, stringNumber, buff, i, param, &writeCount, fh, localProblemId, threadId);
    }
    timeEnd = omp_get_wtime();

    if (LOG_TIMES) addLog(logs, processProblemIndex, timeStart, timeEnd, param, threadId, localProblemId, param->nbSolutionDouble, writeCount, WRITE, writeTime);

    //closes the local problem solution file
    if (!USE_MPI_IO){
        timeStart = omp_get_wtime();
        fclose(fnsca);
        timeEnd = omp_get_wtime();
        closeCount++;
        if (LOG_TIMES) addLog(logs, processProblemIndex, timeStart, timeEnd, param, threadId, localProblemId, 0, 1, CLOSE, 0);
    }

    //resets the buffer
    if (USE_BUFFERING) {
        buff->size = 0;
        buff->previous = 0;
        buff->data[0] = '\0';
    }

}

void writeLogs(opIO *logs, parameters_t param, double outputTime){
    //Creating and opening the cvs file
    FILE *fcsv;
    std::string csvPath = param->logPath;
    csvPath += LOGS_NAME;
    csvPath += std::to_string(param->rank);
    csvPath += ".csv";
    fcsv = fopen(csvPath.c_str(), "w");
    if (!fcsv) errorExit(__LINE__, __FILE__, "Failed to create the process csv file");

    //writing the process logs
    for(int i = 0; i< param->logsSize; i++){
        fprintf(fcsv, "%d,%d,%d,%d,%u,%lf,%lf,%lf,%u,%lf,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%lf\n",
            logs[i].processRank, logs[i].threadId, logs[i].localProblemId, logs[i].type, logs[i].repetition,
            logs[i].elapsedTime, logs[i].startTime, logs[i].endTime, logs[i].write_size, logs[i].writeTime,
            param->ccross,param->subelem, USE_BINARY, USE_BUFFERING, BUFFER_SIZE, USE_MPI_IO,SET_MPI_IO_COLLECTIVE,
            LOW_RAM, LOG_PRECISE_WRITE, omp_get_max_threads(), outputTime);
    }

    fclose(fcsv);
}

//Merges all the individual processes log file into a single one. Doesn't work on log files that doesn't have the same length. 
void mergeLogs(parameters_t param){

    //Create the name and open the main log file
    FILE * fcvsFinal;
    std::string logFinalPath = param->logPath;
    logFinalPath += LOGS_NAME;
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    char timeString[100];
    strftime(timeString, 100, "%j%H%M%S", &tm);
    logFinalPath += timeString;
    logFinalPath += ".csv";

    fcvsFinal = fopen(logFinalPath.c_str(), "w");
    if (fcvsFinal == NULL) errorExit(__LINE__, __FILE__, "Failed to create the main csv file");

    //Prints the columns headers
    fprintf(fcvsFinal, "Process,ThreadId,localProblemID,IOoperation,Iteration,ElapsedTime,StartTime,EndTime,BytesWritten,WriteTime,"
                        "ccross,subelement,binary,buffering,bufferSize,MPI-IO,MPICollective,lowRAM,PreciseWriteTime,OPMThreads,outputPhaseTime\n");


    //Opens every log file inside an array
    FILE **fileArray = (FILE **) malloc(sizeof(FILE *)*param->totalRank);
    if (!fileArray) errorExit(__LINE__, __FILE__, "Malloc Failed");  
    for(int i=0; i<param->totalRank;i++){
        std::string logsPath = param->logPath;
        logsPath += LOGS_NAME;
        logsPath += std::to_string(i);
        logsPath += ".csv";
        fileArray[i] = fopen(logsPath.c_str(), "r");
        if (!fileArray[i]) errorExit(__LINE__, __FILE__, "Malloc Failed");
    }

    //Copies the content of each log file into the main file
    char c;
    for(int i=0; i<param->totalRank;i++){
        while ((c = fgetc(fileArray[i])) != EOF)
            fputc(c, fcvsFinal);
    }


    //Closes every log final in the array
    for(int i=0; i<param->totalRank;i++){
        if (fclose(fileArray[i]) == EOF) fprintf(stderr, "File closing failed for merging\n");
        std::string logsPath = param->logPath;
        logsPath += LOGS_NAME;
        logsPath += std::to_string(i);
        logsPath += ".csv";
        if (remove(logsPath.c_str()) != 0){
            fprintf(stderr, "Couldn't remove the log file %d for merging\n", i);
        }
    }

    fclose(fcvsFinal);
    free(fileArray);
}

//calculating the number of doubles needed for each solution : (2^(log2(subelement)+1) triangular number)
uint computeDataSize(int subelement){
    int n = log2(subelement)+2;
    float f = 3.0;
    f = f/8;
    float ff = 1.0;
    ff = ff/32;
    return (unsigned long) ((f*pow(2, n)) + (ff*pow(4, n))) + 1;
}

void destroyBuffer(buffer **buffers){
    int total_threads = omp_get_max_threads();
    for(int i = 0; i<total_threads; i++){
        free(buffers[i]->data);
        free(buffers[i]);
    }
    free(buffers);
}

void usage(char * programName){
    printf("%s ccross subelements output_path logs_path\n", programName);
    printf("example : %s 5 64 ./tree/ ./logs/\n", programName);
}

int main(int argc, char ** argv){

    //! [Configuring MPI]
    int mpi_rank, mpi_total_rank, required = MPI_THREAD_MULTIPLE, provided = -1;
    MPI_Init_thread(&argc, &argv, required, &provided);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_total_rank);

    if(argc != 5){
        fprintf(stderr, "Incorrect arguments !\n");
        usage(argv[0]);
        return EXIT_FAILURE;
    }
    if (mpi_rank == 0) printf("Starting !\n");
    //Initializing the random seed
    srand (static_cast <unsigned> (time(0) + mpi_rank));

    double timeStart, timeEnd, timeOutputStart, timeOutputEnd;

    //Getting the parameters
    int ccross = atoi(argv[1]);
    int subelem = atoi(argv[2]);
    parameters_t param = (parameters_t) malloc(sizeof(struct parameters));
    if (!param) errorExit(__LINE__, __FILE__, "Malloc Failed");
    param->ccross = ccross;
    param->subelem = subelem;
    param->mainPath = argv[3];
    param->logPath = argv[4];

    param->nbSolutionDouble = computeDataSize(subelem);
    param->nbLocalProblems = pow(4,ccross);
    if (USE_BINARY) param->stripeCountPerProblem = ((sizeof(double) * param->nbSolutionDouble) / (STRIPE_SIZE)) +1;
    else param->stripeCountPerProblem = ((FORMATED_DOUBLE_LENGHT * param->nbSolutionDouble) / (STRIPE_SIZE)) +1;
    param->rank = mpi_rank;
    param->totalRank = mpi_total_rank;
    if(param->totalRank > param->nbLocalProblems){
        fprintf(stderr, "Warning : There is more processes than problems to write\nThis can cause problems, stopping execution\n");
        return EXIT_FAILURE;
    }


    if (mpi_rank==0){
        printf("ccross %d, subelem %d\n", param->ccross, param->subelem);
        printf("tree output : %s\n", param->mainPath);
        printf("Number of doubles for each problem %ld\n", param->nbSolutionDouble);
        if (USE_BINARY) printf("estimated single solution size %ldK\nestimated total size : %ldK\n", (param->nbSolutionDouble*sizeof(double))/1024,(param->nbSolutionDouble*sizeof(double)*param->nbLocalProblems)/1024);
        else printf("estimated single solution size %ldK\nestimated total size : %ldK\n", (param->nbSolutionDouble*FORMATED_DOUBLE_LENGHT)/1024,(param->nbSolutionDouble*FORMATED_DOUBLE_LENGHT*param->nbLocalProblems)/1024);
        //creating the main folders
        mkdir(param->mainPath, S_IRWXU);
        mkdir(param->logPath, S_IRWXU);
    }    
    MPI_Barrier(MPI_COMM_WORLD);


    //allocating an array to log the IO operations
    opIO *logs;
    if (LOG_TIMES){
        if (USE_MPI_IO) param->logsSize = param->nbLocalProblems/param->totalRank + 2; // +2 because we only have 1 opening and closing 
        else param->logsSize = param->nbLocalProblems/param->totalRank * 3; //For each problem we open+write+close
        logs = (opIO *) malloc(sizeof(opIO) * param->logsSize); 
        if (!logs) errorExit(__LINE__, __FILE__, "Malloc Failed for logs");
    }

    
    MPI_Barrier(MPI_COMM_WORLD);
    //allocating and filling an array of arrays with random doubles
    double **arrayRandom = createRandomArray(param);

    //creating each subfolders if using multiple solution files
    if (!USE_MPI_IO){
        #pragma omp parallel for
        for(int i = 0; i< param->nbLocalProblems; i++){
            if ((i+1)%param->totalRank == mpi_rank){
                std::string localProblemPath;
                localProblemPath += param->mainPath;
                localProblemPath += std::to_string(i);
                mkdir(localProblemPath.c_str(),S_IRWXU);
            }
        }
    }



    MPI_Barrier(MPI_COMM_WORLD);
    if (mpi_rank == 0) printf("Starting output phase\n");
    timeOutputStart = omp_get_wtime();


    //opening the single solution file if MPI IO is enabled
    MPI_File fh;
    if (USE_MPI_IO){
        std::string solPath = param->mainPath ;
        solPath += SOLUTION_NAME;
        solPath += ".nsca";

        timeStart = omp_get_wtime();
        MPI_File_open(MPI_COMM_WORLD, solPath.c_str(), MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &fh);
        if (fh == NULL){
            fprintf(stderr, "failed to open the solution file\n");
            return EXIT_FAILURE;
        }
        timeEnd = omp_get_wtime();
        if(LOG_TIMES) addLog(logs, -1, timeStart, timeEnd, param, omp_get_thread_num(), -1, 0, 1, OPEN, 0);
    }

    //Allocating a buffer for each threads
    buffer **threadsBuffer = (buffer**)malloc(sizeof(buffer*)*omp_get_max_threads());
    if (!threadsBuffer) errorExit(__LINE__, __FILE__, "Malloc Failed threads buffer struct");
    if (USE_BUFFERING){
        for(int i = 0; i<omp_get_max_threads(); i++){
            threadsBuffer[i] = (buffer *)malloc(sizeof(buffer));
            threadsBuffer[i]->size = 0;
            threadsBuffer[i]->previous = 0;
            threadsBuffer[i]->max = BUFFER_SIZE;
            threadsBuffer[i]->data = (char *)malloc(sizeof(char) * BUFFER_SIZE + 20);  //adding 20 so that adding to a nearly full buffer doesn't go over the allocated size
            if(!threadsBuffer[i]->data) errorExit(__LINE__, __FILE__, "Malloc Failed individual thread char buffer");
            threadsBuffer[i]->data[0] = '\0';
        }
    }
    

    int rankProblemsStart = (param->nbLocalProblems/param->totalRank) * mpi_rank; 
    int rankProblemsEnd = (param->nbLocalProblems/param->totalRank) * (mpi_rank+1); 
    //Shares all the local problems between processes. Distribution is by chunks (process 0 gets problems 0,1,..,n  process 1 gets problems n+1,n+2,..)
    //then writes the solution for each problem the process has.
    #pragma omp parallel for 
    for(int i = rankProblemsStart; i< rankProblemsEnd; i++){
        uint processProblemIndex = rankProblemsEnd - i - 1;
        if (LOW_RAM) {
            solutionLocalProblem(fh, logs, param, arrayRandom[0], i, processProblemIndex, threadsBuffer[omp_get_thread_num()], omp_get_thread_num());
        }else{
            solutionLocalProblem(fh, logs, param, arrayRandom[processProblemIndex], i, processProblemIndex, threadsBuffer[omp_get_thread_num()], omp_get_thread_num());
        }
    }


    //closing the single solution file if MPI was enabled
    if (USE_MPI_IO){
        timeStart = omp_get_wtime();
        MPI_File_close(&fh);
        timeEnd = omp_get_wtime();
        if (LOG_TIMES) addLog(logs, -1, timeStart, timeEnd, param, omp_get_thread_num(), -1, 0, 1, CLOSE, 0);
    }            




    MPI_Barrier(MPI_COMM_WORLD);
    timeOutputEnd = omp_get_wtime();
    double finalTime = timeOutputEnd - timeOutputStart; 
    if (mpi_rank == 0){
        printf("Finished output phase\n");
        printf("Output took : %f \n", finalTime);
    }

    //writes the logs then merges them in a single file
    if (LOG_TIMES){
        if (mpi_rank == 0) printf("Starting to write logs\n");
        writeLogs(logs, param, finalTime);
        MPI_Barrier(MPI_COMM_WORLD);
        if (mpi_rank == 0) mergeLogs(param);
        if (mpi_rank == 0) printf("Logs written\n");
        free(logs);
    }


    if (USE_BUFFERING) destroyBuffer(threadsBuffer);
    destroyArray(arrayRandom, param);
    free(param);

    //! [Releasing MPI]
    MPI_Finalize();
    return EXIT_SUCCESS;
}
MSLIO is an I/O kernel, created to mimic the I/O behavior of multiscale simulations. More specifically, it focuses on the output routines. It has been created based on the MHM library. Additional information can be obtained from our paper:

- Francieli Boito, Antônio Tadeu Gomes, Louis Peyrondet and Luan Teylo, "I/O performance of multiscale finite element simulations on HPC environments", in 13th Workshop on Applications for Multi-Core Architectures (WAMCA 2022), Bordeaux, France.

You are welcome to use this software and we hope it is useful to you. If you do, please consider citing our work.

# Credits

MSLIO has been mainly developped by Louis Peyrondet during his 2022 summer internship in Inria Bordeaux, while working with Francieli Boito, Antônio Tadeu Gomes, Luan Teylo and Alexis Bandet. This research was in the context of the [https://team.inria.fr/hpcprosol/](HPCProSol) joint team between Inria and LNCC. The involved organizations are: 

- University of Bordeaux, CNRS, Bordeaux INP, INRIA, LaBRI, France
- Laboratório Nacional de Computação Científica (LNCC), Brazil

# Compilation

The only dependency is MPI. So far MSLIO was tested (and worked) with OpenMPI 3.1.4 and gcc 9.3.0.

    mpiCC mslio.cpp -fopenmp -O3 -o mslio -lm

## Modes

You can change the kernel behaviour by enabling/disabling different options in the #define lines

| Define | Description |
| ----------- | ----------- |
| USE_BUFFERING | Uses a buffer to store the data before writing it to the file |
| BUFFER_SIZE | Size in bytes of the buffer |
| USE_MPI_IO | Instead of making 1 solution file per local problem, uses MPI_File_write_at() to only use 1 shared solution file for all the local problems   |
| SET_MPI_IO_COLLECTIVE | Change MPI_File_write_at() to MPI_File_write_at_all(), the collective version |
| USE_BINARY | Instead of writing formated doubles, writes the double in binary |
| LOG_TIMES | Creates a CSV log file of the IO operations details |
| LOG_PRECISE_WRITE | Adds the PreciseWriteTime field of the CSV |
| LOGS_NAME | Name of the log file. The complete name of the file will be LOGS_NAME+DayOfTheYear+Hours+Minutes+Seconds |
| LOW_RAM | Instead of allocating an array per local problem, limits it to only 1 array per process |
| SOLUTION_NAME | Name of the solution file. The complete name of the file will be SOLUTION_NAME.nsca |
| STRIPE_PADDING | If using a Parallel File System, make sure that each local problem have its own stripe(s) to prevent threads from writing at the same time on the same stripe. |
| STRIPE_SIZE | The size of the stripe |

# Usage

    ./mslio ccross subelement outputPath logOutputPath

Ccross represent the number of local problems. With ccross 4 : 4^4 local problems, ccross 5 : 4^5 local problems ....

Subelement is the amount of data written for each solution. It should be increased in powers of 2. The increase of data is calculated as 2^(log2(subelment)+1) triangular number (https://oeis.org/A028401). More details can be found in the paper mentioned above.

outputPath should be the folder where the solutions will be written.

logOutputPath should be the folder where the log files will be written.


# CSV Structure

For every open/close/write a csv line is generated. (Write are grouped for 1 whole local problem solution) 

Eg: if you have 16 local problems using 1 file per problem then you will have 16 opens (opening each file), 16 closes (closing each file), and 16 writes (writing each file, but writing each file may require multiple writes calls that would increase the Iteration field)  

## All of these fields change for each IO operation
| Field | Description |
| ----------- | ----------- |
| Process         |   The mpi process that made the operation |
| ThreadId        |   The thread from within the process that made the operation
| localProblemID  |   Id of the local problem computed. Value is between [0,4^ccross[
| IOoperation     |   0 = Open, 1 = Close, 2 = Write 
| Iteration       |   Number of time the operation was repeated for this local problem. (eg : 200 Iteration for a Write operation means that to write the solution for 1 local problem, it took 200 calls to write)
| ElapsedTime     |   The real time taken from the start of the localProblemWriting function to its end. Meaning that it also includes times from formatting, memcpy, etc  that are within the localProblemWriting function. 
| StartTime       |   The time the IOoperation started
| EndTime         |   The time the IOoperation ended
| BytesWritten    |   The total amount of bytes written for this local problem 
| WriteTime       |   If enabled will only get the time spent inside the fwrite\|fprintf\|MPI-IO functions without the times from formatting, memcpy, etc. This will slow down the execution as there can be a lot of single calls


## All of these fields are constant for a execution
| Field | Description |
| ----------- | ----------- |
| ccross          |   The ccross parameter that was used
| subelement      |   The subelement parameter that was used
| binary          |   If the binary mode was enabled
| buffering       |   If the buffer mode was enabled
| bufferSize      |   The max buffer size set as parameter 
| MPI-IO          |   If the MPI-IO mode was enabled (using noncollective write functions)
| MPICollective   |   If MPI-IO was using the collective write functions 
| lowRAM          |   If using the lowRAM mode
| PreciseWriteTime |   If the PreciseWriteTime mode was enabled. 
| OPMThreads      |   The maximum amount of OMP threads given.

## This field only changes between process.
| Field | Description |
| ----------- | ----------- |
| outputPhaseTime |   The real time elapsed from the start of process output phase to its end.  

# TO DO

As future work, we would like to extend MSLIO to perform periodic output phases. For now it consists of one I/O phase only.

